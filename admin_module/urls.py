from django.urls import include, path

from admin_module.views import get_report_comment, comment_details, delete_comment, dismiss_report
from admin_module.views import get_report_publication, dismiss_pub_report, publication_details
from admin_module.views import delete_publication, get_transactions, get_disabled_publications
from admin_module.views import delete_disabled_publications, delete_all_disabled_publications

urlpatterns = [
    path(
        'comment-delete/<uuid:comment_pk>/<uuid:report_pk>/',
        delete_comment,
        name='comment-delete',
    ),
    path(
        'comment-details/<uuid:comment_pk>/<uuid:report_pk>/',
        comment_details,
        name='comment-details',
    ),
    path(
        'comment-reports/',
        get_report_comment,
        name='comment-reports',
    ),
    path(
        'comment-reports-dismiss/<uuid:pk>',
        dismiss_report,
        name='comment-reports-dismiss',
    ),
    path(
        'publication-delete/<uuid:publication_pk>/<uuid:report_pk>/',
        delete_publication,
        name='publication-delete',
    ),
    path(
        'publication-details/<uuid:publication_pk>/<uuid:report_pk>/',
        publication_details,
        name='publication-details',
    ),
    path(
        'publication-reports/',
        get_report_publication,
        name='publication-reports',
    ),
    path(
        'publication-reports-dismiss/<uuid:pk>',
        dismiss_pub_report,
        name='publication-reports-dismiss',
    ),
    path(
        'transaction-history-all/',
        get_transactions,
        name='transaction-history-all',
    ),
    path(
        'disabled-publications',
        get_disabled_publications,
        name='disabled-publications',
    ),
    path(
        'delete-disabled-publications/<uuid:pk>/',
        delete_disabled_publications,
        name='delete-disabled-publications',
    ),
    path(
        'delete-all-disabled-publications',
        delete_all_disabled_publications,
        name='delete-all-disabled-publications',
    ),    
]
