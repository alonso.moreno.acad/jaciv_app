
from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from publications.models import Publication, PublicationWarehouse
from users.models import Report, Comment, CustomUser, Transaction


@staff_member_required(login_url='login')
def get_report_comment(request):
    comment_reports = Report.objects.filter(
        category='C').order_by('-emission_date')

    paginator = Paginator(comment_reports, 10)
    page = request.GET.get('page')
    reports = paginator.get_page(page)

    context = {
        'reports': reports,
    }

    return render(request, 'admin_module/comment_reports.html', context=context)


@staff_member_required(login_url='login')
def comment_details(request, comment_pk, report_pk):
    comment = get_object_or_404(Comment, pk=comment_pk)
    report = get_object_or_404(Report, pk=report_pk)

    context = {
        'comment': comment,
        'report': report,
    }

    return render(request, 'admin_module/comment_details.html', context=context)


@staff_member_required(login_url='login')
def delete_comment(request, comment_pk, report_pk):
    comment = get_object_or_404(Comment, pk=comment_pk)
    current_report = get_object_or_404(Report, pk=report_pk)
    reports = Report.objects.filter(message=current_report.message)

    if reports is not None:
        for report in reports:
            report.delete()

    current_report.delete()
    comment.delete()

    return redirect('comment-reports')


@staff_member_required(login_url='login')
def dismiss_report(request, pk):
    report = get_object_or_404(Report, pk=pk)
    report.delete()

    return redirect('comment-reports')


@staff_member_required(login_url='login')
def get_report_publication(request):
    publication_reports = Report.objects.filter(
        category='P').order_by('-emission_date')

    paginator = Paginator(publication_reports, 10)
    page = request.GET.get('page')
    reports = paginator.get_page(page)

    context = {
        'reports': reports,
    }

    return render(request, 'admin_module/publication_reports.html', context=context)


@staff_member_required(login_url='login')
def publication_details(request, publication_pk, report_pk):
    publication = get_object_or_404(Publication, pk=publication_pk)
    report = get_object_or_404(Report, pk=report_pk)

    user = None

    if publication is not None:
        user = CustomUser.objects.get(pk=publication.seller.id)

    context = {
        'publication': publication,
        'report': report,
        'seller': user,
    }

    return render(request, 'admin_module/publication_details.html', context=context)


@staff_member_required(login_url='login')
def delete_publication(request, publication_pk, report_pk):
    publication = get_object_or_404(Publication, pk=publication_pk)
    current_report = get_object_or_404(Report, pk=report_pk)
    reports = Report.objects.filter(message=current_report.message)

    if reports is not None:
        for report in reports:
            report.delete()

    current_report.delete()
    publication.delete()

    return redirect('publication-reports')


@staff_member_required(login_url='login')
def dismiss_pub_report(request, pk):
    report = get_object_or_404(Report, pk=pk)
    report.delete()

    return redirect('publication-reports')


@staff_member_required(login_url='login')
def get_transactions(request):
    user = request.user
    transactions = Transaction.objects.all()
    context = {
        'user': user,
        'transactions': transactions,
    }

    return render(request, 'admin_module/transaction_history.html', context=context)


@staff_member_required(login_url='login')
def get_disabled_publications(request):
    disabled_publications = Publication.objects.filter(state='N')

    paginator = Paginator(disabled_publications, 15)
    page = request.GET.get('page')
    publications = paginator.get_page(page)

    context = {
        'publications': publications,
    }

    return render(request, 'admin_module/disabled_publications.html', context=context)


@staff_member_required(login_url='login')
def delete_disabled_publications(request, pk):

    disabled_publication = get_object_or_404(Publication, pk=pk)

    deletion_motive = 'O'
    deletion_date = timezone.now()
    active_days = get_days_passed(
        disabled_publication.publication_date, deletion_date)

    publication_old = PublicationWarehouse.objects.create(
        product_name=disabled_publication.product_name,
        price=disabled_publication.price,
        seller=disabled_publication.seller,
        publication_date=disabled_publication.publication_date,
        removal_date=deletion_date,
        active_days=active_days,
        removal_motive=deletion_motive,
    )
    publication_old.save()
    disabled_publication.delete()

    return redirect('disabled-publications')


@staff_member_required(login_url='login')
def delete_all_disabled_publications(request):

    disabled_publications = Publication.objects.filter(state='N')

    for disabled_publication in disabled_publications:

        deletion_motive = 'O'
        deletion_date = timezone.now()
        active_days = get_days_passed(
            disabled_publication.publication_date, deletion_date)

        publication_old = PublicationWarehouse.objects.create(
            product_name=disabled_publication.product_name,
            price=disabled_publication.price,
            seller=disabled_publication.seller,
            publication_date=disabled_publication.publication_date,
            removal_date=deletion_date,
            active_days=active_days,
            removal_motive=deletion_motive,
        )
        publication_old.save()
        disabled_publication.delete()

    return redirect('disabled-publications')


def get_days_passed(before, after):
    d1 = before
    d2 = after.date()
    return abs(d1 - d2).days
