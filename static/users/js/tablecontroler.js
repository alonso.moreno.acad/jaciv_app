$(document).ready(function() {
    $('#transaction-table').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ elementos por página",
            "zeroRecords": "Aún no has realizado una compra, qué esperas !",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay transacciones disponibles",
            "infoFiltered": "(filtrado de _MAX_ total elementos)",
            "search": "Buscar",
            "paginate": {
                "previous": "Atrás",
                "next": "Siguiente"
            }
        }
    });
})