import re

from users.models import CustomUser


def check_mobile_phone_integrity(mobile_phone):

    mobile_phone = re.match('^(044)*[0-9]{10}$', mobile_phone)
    if mobile_phone is None:
        return False
    else:
        return True


def check_email_integrity(data):

    existing_emails = CustomUser.objects.filter(email=data)

    if len(data) <= 0:
        return False
    else:
        if len(existing_emails) > 0:
            return False
        else:
            return True
