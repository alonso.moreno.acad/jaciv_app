import re

from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.core.exceptions import ValidationError

from string import Template

from users.models import CustomUser, Report, Comment, Suggestion, Transaction, Raiting
from users.utils import check_mobile_phone_integrity, check_email_integrity


class DateInput(forms.DateInput):
    input_type = 'date'


class CustomUserCreationForm(UserCreationForm):
    class Meta (UserCreationForm):
        model = CustomUser
        fields = ['username', 'first_name', 'last_name', 'email', 'mobile_phone',
                  'birth_date', 'sex', 'terms_accepted', ]
        widgets = {
            'birth_date': DateInput(),
        }
        help_texts = {
            'email': _('email@example.com'),
            'mobile_phone': _('Por ejemplo: 5548136925'),
        }

    def __init__(self, *args, **kwargs):
        super(CustomUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields['terms_accepted'].required = True

    def clean_mobile_phone(self):
        data = self.cleaned_data['mobile_phone']
        mobile_phone = check_mobile_phone_integrity(data)

        if mobile_phone is False:
            raise ValidationError(
                _('Número invalido: Utilice 0445512345678 (13 dígitos) ó 5512345678 (10 dígitos)'))

        return data

    def clean_email(self):
        data = self.cleaned_data['email']
        emails = CustomUser.objects.filter(email=data)
        if len(emails) > 0:
            raise ValidationError(
                _('El correo ingresado ya está en uso, intente con otro correo.')
            )

        return data


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ['email', 'profile_picture', 'mobile_phone', ]

    def clean_mobile_phone(self):
        data = self.cleaned_data['mobile_phone']
        mobile_phone = check_mobile_phone_integrity(data)

        if mobile_phone is False:
            raise ValidationError(
                _('Número invalido: Utilice 0445512345678 (13 dígitos) ó 5512345678 (10 dígitos)'))

        return data


class AccountDetailsForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['email', 'profile_picture', 'mobile_phone', ]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(AccountDetailsForm, self).__init__(*args, **kwargs)

    def clean_mobile_phone(self):
        data = self.cleaned_data['mobile_phone']
        mobile_phone = check_mobile_phone_integrity(data)

        if mobile_phone is False:
            raise ValidationError(
                _('Número invalido: Utilice 0445512345678 (13 dígitos) ó 5512345678 (10 dígitos)'))

        return data

    def clean_email(self):
        data = self.cleaned_data['email']
        data = data.strip()

        if self.user and self.user.email == data:
            return data
        else:
            valid_email = check_email_integrity(data)
            if not valid_email:
                raise ValidationError(
                    _('El correo ingresado es inválido o ya está en uso'))

        return data


class ReportCreationForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ['sender', 'recipient', 'slug', 'message', 'category', ]

    def __init__(self, *args, **kwargs):
        super(ReportCreationForm, self).__init__(*args, **kwargs)
        self.fields['sender'].widget = forms.HiddenInput()
        self.fields['recipient'].widget = forms.HiddenInput()
        self.fields['category'].widget = forms.HiddenInput()


class SuggestionCreationForm(forms.ModelForm):
    class Meta:
        model = Suggestion
        fields = ['sender', 'recipient', 'slug', 'message', ]

    def __init__(self, *args, **kwargs):
        super(SuggestionCreationForm, self).__init__(*args, **kwargs)
        self.fields['sender'].widget = forms.HiddenInput()
        self.fields['recipient'].widget = forms.HiddenInput()


class CommentCreationForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['sender', 'recipient', 'message', ]

    def __init__(self, *args, **kwargs):
        super(CommentCreationForm, self).__init__(*args, **kwargs)
        self.fields['sender'].widget = forms.HiddenInput()
        self.fields['recipient'].widget = forms.HiddenInput()
        self.fields['message'].label = False


class RaitingCreationForm(forms.ModelForm):
    class Meta:
        model = Raiting
        fields = ('grader', 'reciever', 'grade')

        widgets = {
            'grade': forms.NumberInput(attrs={'id': 'gradePicker', 'readonly': True, })
        }

    def __init__(self, *args, **kwargs):
        super(RaitingCreationForm, self).__init__(*args, **kwargs)
        self.fields['grader'].widget = forms.HiddenInput()
        self.fields['reciever'].widget = forms.HiddenInput()
        self.fields['grade'].label = False

    def clean_grade(self):
        data = self.cleaned_data['grade']

        if data < 0:
            raise ValidationError(
                _('El número ingresado no puede ser menor a 0'))

        if data > 10:
            raise ValidationError(
                _('El número ingresado no puede ser mayor a 10'))

        return data


class TransactionCreationForm(forms.Form):
    class Meta:
        model = Transaction
        fields = ('publication_number', 'total_price', 'buyer')
