import json
import paypalrestsdk
import re
import requests
from requests.auth import HTTPBasicAuth
from datetime import datetime, timedelta
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from itertools import chain
from jaciv_app import settings

from publications.models import Deal, Publication, Tag

from users.forms import CustomUserCreationForm, CustomUserChangeForm, AccountDetailsForm, CommentCreationForm
from users.forms import RaitingCreationForm, ReportCreationForm, SuggestionCreationForm
from users.models import CustomUser, Transaction, Comment, Raiting, Report, Suggestion


def welcome(request):
    latest_publications = Publication.objects.all().order_by(
        '-publication_date')[:3]
    most_viewed_publications = Publication.objects.all().order_by('-views')[:3]

    context = {
        'latest_publications': latest_publications,
        'most_viewed_publications': most_viewed_publications,
    }

    return render(request, 'home.html', context=context)


class Signup(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'users/signup.html'


@login_required(login_url='login')
def account_management(request, pk):
    user = request.user

    form = AccountDetailsForm(initial={
        'email': user.email,
        'profile_picture': user.profile_picture,
        'mobile_phone': user.mobile_phone,
    })

    user_comments = Comment.objects.filter(
        recipient=user).order_by('-emission_date')

    context = {
        'form': form,
        'user': user,
        'comments': user_comments,
    }

    return render(request, 'users/account_management.html', context=context)


@login_required(login_url='login')
def report_comment(request, pk):
    user = request.user
    comment = Comment.objects.get(pk=pk)

    report = Report.objects.create(
        sender=user,
        recipient=comment.sender,
        slug='Reporte de Comentario',
        message=comment.id,
        category='C'
    )

    report.save()
    return redirect('account-management', pk=user.id)


@login_required(login_url='login')
def update_account_details(request):
    if request.method == 'POST':
        form = AccountDetailsForm(
            request.POST, request.FILES, user=request.user)

        if form.is_valid():
            user = request.user
            user.email = form.cleaned_data['email']
            user.mobile_phone = form.cleaned_data['mobile_phone']
            profile_picture = form.cleaned_data['profile_picture']

            if profile_picture == user.profile_picture:
                user.save()
            else:
                if profile_picture is None or profile_picture is False:
                    profile_picture = user.profile_picture
                user.profile_picture = profile_picture
                user.save()

            return redirect('account-management', pk=user.id)

        else:
            user = request.user
            user_comments = Comment.objects.filter(
                recipient=user).order_by('-emission_date')
            form.initial = {
                'email': user.email,
                'profile_picture': user.profile_picture,
                'mobile_phone': user.mobile_phone,
            }
            error_message = 'Error al actualizar el perfil.'
            context = {
                'error_message': error_message,
                'form': form,
                'user': user,
                'comments': user_comments
            }
            return render(request, 'users/account_management.html', context=context)

    else:
        user = request.user
        form = AccountDetailsForm(initial={
            'email': user.email,
            'profile_picture': user.profile_picture,
            'mobile_phone': user.mobile_phone,
        }, user=request.user)
        context = {
            'form': form,
            'user': user,
        }

        return render(request, 'users/account_management.html', context=context)


@login_required(login_url='login')
def delete_account(request):
    user = request.user
    user.delete()
    return redirect('login')


@login_required(login_url='login')
def account_suggestions(request):
    user = request.user
    user_suggestions = Suggestion.objects.filter(
        recipient=user).order_by('-emission_date')
    user_suggestions_count = user_suggestions.count()

    context = {
        'suggestions': user_suggestions,
        'total_suggestion': user_suggestions_count,
    }

    return render(request, 'users/account_suggestions.html', context=context)


@login_required(login_url='login')
def delete_suggestion(request, pk):
    suggestion = Suggestion.objects.get(pk=pk)
    suggestion.delete()

    return redirect('user-suggestions')


@login_required(login_url='login')
def transaction_history(request):
    user = request.user
    transactions = Transaction.objects.filter(
        buyer_id=user.id).filter(payment_status=True)

    context = {
        'user': user,
        'transactions': transactions,
    }

    return render(request, 'users/transaction_history.html', context=context)


@login_required(login_url='login')
def refund_transaction(request, pk):
    transaction = get_object_or_404(Transaction, pk=pk)
    user = request.user

    total_price = float(transaction.total_price)
    total_price = total_price - float((total_price * 2.4) + 5.69)

    paypalrestsdk.configure({
        'mode': settings.PAYPAL_MODE,
        'client_id': settings.PAYPAL_CLIENT_ID,
        'client_secret': settings.PAYPAL_CLIENT_SECRET,
    })

    paypal_payment = paypalrestsdk.Payment.find(transaction.paypal_id)
    string_payment = str(paypal_payment)
    string_payment = string_payment.split("related_resources")
    string_payment = string_payment[1].split('state')
    string_payment = string_payment[0].replace('{', '')
    string_payment = string_payment.replace('}', '')
    string_payment = string_payment.split('id')
    string_payment = string_payment[1]
    string_payment = string_payment.replace(':', '')
    string_payment = string_payment.replace("'", '')
    string_payment = string_payment.replace(",", '')
    string_payment = string_payment.strip()

    paypal_sale = paypalrestsdk.Sale.find(string_payment)
    refund = paypal_sale.refund({})

    message = ''
    success = False

    if refund.success():
        transaction_date = transaction.transaction_date_time
        limit_date = transaction_date + timedelta(days=1)

        created_publications = Publication.objects.filter(
            publication_date__range=[transaction_date, limit_date]).count()

        if created_publications > 0:
            remove_publications = Publication.objects.filter(
                publication_date__range=[transaction_date, limit_date])
            for publication in remove_publications:
                publication.delete()

        user.available_publications = user.available_publications - \
            (transaction.publication_number - created_publications)
        user.save()

        transaction.payment_status = False
        transaction.save()

        message = 'Reembolso realizado con éxito'
        success = True

    else:
        message = 'Ocurrio un error por parte de PayPal, favor de intentarlo más tarde, o contacte directamente a Paypal.'

    current_transactions = Transaction.objects.filter(
        buyer_id=user.id).filter(payment_status=True)

    context = {
        'user': user,
        'transactions': current_transactions,
        'message': message,
        'success': success,
    }

    return render(request, 'users/transaction_history.html', context=context)


def search_store(request):
    if request.method == 'POST':
        category = request.POST.get('category')
        search = request.POST.get('search')

        if search is None or len(search) <= 0:
            return redirect('welcome')

        if category == 'Cuentas':
            accounts_list = CustomUser.objects.filter(
                Q(username=search) |
                Q(username__icontains=search) |
                Q(first_name=search) |
                Q(first_name__icontains=search) |
                Q(last_name=search) |
                Q(last_name__icontains=search)
            ).distinct()

            paginator = Paginator(accounts_list, 25)
            page = request.GET.get('page')
            accounts = paginator.get_page(page)

            context = {
                'accounts': accounts,
            }

            return render(request, 'users/accounts_search.html', context=context)

        elif category == 'Productos':

            context = search_products_context(request, search, category)

            return render(request, 'publications/publication_search.html', context=context)

        else:
            pass

    else:
        return render(request, 'home.html')


def filter_search(request):

    if request.method == 'POST':

        category = request.POST.get('category')
        search = request.POST.get('search')
        price = request.POST.get('price')
        date = request.POST.get('date')
        tag = request.POST.get('tag')

        context = search_products_context(
            request, search, category, tag=tag, price=price, date=date)

        return render(request, 'publications/publication_search.html', context=context)

    else:
        return render(request, 'home.html')


@login_required(login_url='login')
def view_profile(request, pk):
    context = get_profile_context(request, pk)

    return render(request, 'users/user_profile.html', context=context)


@login_required(login_url='login')
def comment_profile(request, profile_pk):
    if request.method == 'POST':
        form = CommentCreationForm(request.POST)

        if form.is_valid():
            user_sender = form.cleaned_data['sender']
            user_recipient = form.cleaned_data['recipient']
            comment_text = form.cleaned_data['message']

            new_comment = Comment.objects.create(
                sender=user_sender,
                recipient=user_recipient,
                message=comment_text
            )
            new_comment.save()

            return redirect('user-account', pk=user_recipient.id)

        else:
            form = CommentCreationForm()
            context = get_profile_context(
                request, profile_pk, error_message='El comentario ingresado no es válido!')

            return render(request, 'users/user_profile.html', context=context)

    else:
        pass


@login_required(login_url='login')
def rate_profile(request):
    if request.method == 'POST':
        form = RaitingCreationForm(request.POST)

        if form.is_valid():
            user_grader = form.cleaned_data['grader']
            user_receiver = form.cleaned_data['reciever']
            new_grade = form.cleaned_data['grade']
            current_rating = Raiting.objects.filter(
                Q(grader=user_grader) &
                Q(reciever=user_receiver)
            ).first()

            if current_rating is not None:
                current_rating = Raiting.objects.filter(
                    Q(grader=user_grader) &
                    Q(reciever=user_receiver)
                ).first()
                current_rating.grade = new_grade
                current_rating.save()

            else:
                new_rating = Raiting.objects.create(
                    grader=user_grader,
                    reciever=user_receiver,
                    grade=new_grade
                )
                new_rating.save()

            return redirect('user-account', pk=user_receiver.id)

        else:
            pass

    else:
        pass


@login_required(login_url='login')
def report_profile(request, profile_pk):
    if request.method == 'POST':

        form = ReportCreationForm(request.POST)

        if form.is_valid():

            report_sender = form.cleaned_data['sender']
            report_recipient = form.cleaned_data['recipient']
            report_slug = form.cleaned_data['slug']
            report_message = form.cleaned_data['message']
            report_category = form.cleaned_data['category']

            new_report = Report.objects.create(
                sender=report_sender,
                recipient=report_recipient,
                slug=report_slug,
                message=report_message,
                category=report_category
            )
            new_report.save()

            return redirect('user-account', pk=report_recipient.id)

        else:
            form = ReportCreationForm()
            context = get_profile_context(
                request, profile_pk, error_message='El reporte ingresado no es válido!')

            return render(request, 'users/user_profile.html', context=context)

    else:
        pass


@login_required(login_url='login')
def suggest_profile(request, profile_pk):
    if request.method == 'POST':

        form = SuggestionCreationForm(request.POST)

        if form.is_valid():

            suggestion_sender = form.cleaned_data['sender']
            suggestion_recipient = form.cleaned_data['recipient']
            suggestion_slug = form.cleaned_data['slug']
            suggestion_message = form.cleaned_data['message']

            new_suggestion = Suggestion.objects.create(
                sender=suggestion_sender,
                recipient=suggestion_recipient,
                slug=suggestion_slug,
                message=suggestion_message,
            )

            return redirect('user-account', pk=suggestion_recipient.id)

        else:
            form = SuggestionCreationForm()
            context = get_profile_context(
                request, profile_pk, error_message='La sugerencia ingresada no es válida!')

            return render(request, 'users/user_profile.html', context=context)

    else:
        pass


def get_profile_context(request, pk, **kwargs):
    user = get_object_or_404(CustomUser, pk=pk)
    user_comments = Comment.objects.filter(
        recipient=user).order_by('-emission_date')
    user_publications = Publication.objects.filter(seller=user)

    user_rating = None
    current_rating_count = Raiting.objects.filter(
        reciever=user).count()

    if current_rating_count > 0:

        current_rating = Raiting.objects.filter(reciever=user)
        user_rating = 0

        for rating in current_rating:
            user_rating = user_rating + rating.grade

        user_rating = user_rating / current_rating_count
        user_rating = round(user_rating, 2)

    else:
        user_rating = 10

    comment_form = CommentCreationForm(initial={
        'sender': request.user,
        'recipient': user,
    })

    rating_form = RaitingCreationForm(initial={
        'grader': request.user,
        'reciever': user,
    })

    report_form = ReportCreationForm(initial={
        'sender': request.user,
        'recipient': user,
        'category': 'U',
    })

    suggestion_form = SuggestionCreationForm(initial={
        'sender': request.user,
        'recipient': user,
    })

    error = None

    if kwargs is not None:
        for key, value in kwargs.items():
            if key == 'error_message':
                error = value

    context = {
        'user': user,
        'comments': user_comments,
        'rate': user_rating,
        'publications': user_publications,
        'comment_form': comment_form,
        'rating_form': rating_form,
        'report_form': report_form,
        'suggestion_form': suggestion_form,
        'error_message': error,
    }

    return context


def search_products_context(request, search, category, **kwargs):
    tag = None
    publication_list = None

    tag_id_list = Tag.objects.filter(
        tag_name__icontains=search.upper()
    ).values_list('id', flat=True)

    tag_id_list = list(tag_id_list)

    if len(tag_id_list) > 0:
        publication_list = Publication.objects.filter(
            Q(product_name=search) |
            Q(product_name__icontains=search) |
            Q(tag_list__id__in=tag_id_list)
        ).distinct()

        for publication in publication_list:
            tags = publication.tag_list.all().values_list('id', flat=True)
            tags = list(tags)
            for tag in tags:
                if tag not in tag_id_list:
                    tag_id_list.append(tag)

    else:
        publication_list = Publication.objects.filter(
            Q(product_name=search) |
            Q(product_name__icontains=search)
        ).distinct()

        for publication in publication_list:
            tags = publication.tag_list.all().values_list('id', flat=True)
            tags = list(tags)
            for tag in tags:
                if tag not in tag_id_list:
                    tag_id_list.append(tag)

    tag_list_raw = Tag.objects.filter(
        pk__in=tag_id_list
    ).distinct()

    price = kwargs.pop('price', None)
    date = kwargs.pop('date', None)
    tag = kwargs.pop('tag', None)

    if tag is not None and tag != '...':

        temp_tag_list = Tag.objects.filter(
            tag_name__icontains=tag).values_list('id', flat=True)

        temp_tag_list = list(temp_tag_list)

        publication_list = Publication.objects.filter(
            Q(product_name=search) |
            Q(product_name__icontains=search) |
            Q(tag_list__id__in=temp_tag_list)
        ).distinct()

    if price is not None and price != '...':
        if price == 'Menor a Mayor':
            publication_list = publication_list.order_by('price')
        elif price == 'Mayor a Menor':
            publication_list = publication_list.order_by('-price')

    if date is not None and date != '...':
        if date == 'Nuevo Primero':
            publication_list = publication_list.order_by('-publication_date')
        elif date == 'Viejo Primero':
            publication_list = publication_list.order_by('publication_date')

    paginator = Paginator(publication_list, 100)
    page = request.GET.get('page')
    publications = paginator.get_page(page)

    context = {
        'tag_list': tag_list_raw,
        'category': category,
        'search': search,
        'publications': publication_list
    }

    return context
