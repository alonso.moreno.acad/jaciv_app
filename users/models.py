import uuid
import datetime
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse


class CustomUser(AbstractUser):
    SEX_CHOICE = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
        ('O', 'Otro'),
    )
    profile_picture = models.ImageField(
        upload_to='images/profile_pics',
        blank=True,
        null=True,
        verbose_name='foto de Perfil',
    )
    mobile_phone = models.CharField(
        max_length=13,
        verbose_name='teléfono',
    )
    sex = models.CharField(
        max_length=1,
        choices=SEX_CHOICE,
        default='O',
        verbose_name='sexo',
    )
    birth_date = models.DateField(
        verbose_name='fecha de Nacimeinto',
    )
    available_publications = models.IntegerField(
        default=0,
        verbose_name='publicaciones Disponibles',
    )
    terms_accepted = models.BooleanField(
        default=False,
        verbose_name='términos y Condiciones Aceptados',
    )

    class Meta:
        indexes = [
            models.Index(fields=['id', ], name='user_id_index'),
            models.Index(fields=['username', ], name='user_username_index'),
            models.Index(fields=['email', ], name='user_email_index'),
        ]
        unique_together = (('username', 'email'),)
        ordering = ['date_joined', ]
        verbose_name = 'usuario'
        verbose_name_plural = 'usuarios'

    def __str__(self):
        return self.email

    def get_absolute_url(self):
        return reverse('user-account', args=[str(self.id)])


class Report(models.Model):
    CATEGORY = (
        ('U', 'Usuario'),
        ('C', 'Comments'),
        ('P', 'Publicación'),
    )

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
    )
    sender = models.ForeignKey(
        CustomUser,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='customuser_as_report_sender',
        verbose_name='remitente',
    )
    recipient = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='customuser_as_report_recipient',
        verbose_name='destinatario',
    )
    slug = models.CharField(
        max_length=250,
        verbose_name='asunto',
    )
    message = models.TextField(
        max_length=750,
        verbose_name='mensaje',
    )
    category = models.CharField(
        max_length=1,
        choices=CATEGORY,
        default='U',
        verbose_name='categoría',
    )
    emission_date = models.DateTimeField(
        default=datetime.datetime.now,
        verbose_name='fecha de Emisión',
    )

    class Meta:
        indexes = [
            models.Index(fields=['id', ], name='report_id_index'),
        ]
        ordering = ['emission_date', 'recipient', ]
        verbose_name = 'reporte'
        verbose_name_plural = 'reportes'

    def __str__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('report-details', args=[str(self.id)])


class Suggestion(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
    )
    sender = models.ForeignKey(
        CustomUser,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='customuser_as_suggestion_sender',
        verbose_name='remitente',
    )
    recipient = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='customuser_as_suggestion_recipient',
        verbose_name='destinatario',
    )
    slug = models.CharField(
        max_length=250,
        verbose_name='asunto',
    )
    message = models.TextField(
        max_length=750,
        verbose_name='mensaje',
    )
    emission_date = models.DateTimeField(
        default=datetime.datetime.now,
        verbose_name='fecha de Emisión',
    )

    class Meta:
        indexes = [
            models.Index(fields=['id', ], name='suggestion_id_index'),
        ]
        ordering = ['emission_date', 'recipient', ]
        verbose_name = 'sugerencia'
        verbose_name_plural = 'sugerencias'

    def __str__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('suggestion-details', args=[str(self.id)])

    def get_delete_url(self):
        return reverse('delete-suggestion', args=[str(self.id)])


class Comment(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
    )
    sender = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='customuser_as_comment_sender',
        verbose_name='remitente',
    )
    recipient = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='customuser_as_comment_recipient',
        verbose_name='destinatario',
    )
    message = models.TextField(
        max_length=250,
        verbose_name='mensaje',
    )
    emission_date = models.DateTimeField(
        default=datetime.datetime.now,
        verbose_name='fecha de Emisión',
    )

    class Meta:
        indexes = [
            models.Index(fields=['id', ], name='comment_id_index'),
        ]
        ordering = ['emission_date', 'recipient', ]
        verbose_name = 'comentario'
        verbose_name_plural = 'comentarios'

    def __str__(self):
        return self.message

    def get_absolute_url(self):
        return reverse('comment', args=[str(self.id)])

    def get_report_comment_url(self):
        return reverse('report-commnet', args=[str(self.id)])


class Raiting(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
    )
    grader = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='customuser_as_grader',
        verbose_name='remitente',
    )
    reciever = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='customuser_as_reciever',
        verbose_name='destinatario',
    )
    grade = models.IntegerField(
        verbose_name='raiting',
    )
    grade_date = models.DateTimeField(
        default=datetime.datetime.now,
        verbose_name='fecha de Evaluación',
    )

    class Meta:
        indexes = (
            models.Index(fields=['id', ], name='raiting_id_index'),
        )
        ordering = ['reciever', 'grade_date', ]
        verbose_name = 'califiacion'
        verbose_name_plural = 'calificaciones'

    def __str__(self):
        return str(self.grade)


class Transaction(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
    )
    publication_number = models.IntegerField(
        verbose_name='número de Publicaciones',
    ) 
    total_price = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        default=0.00,
        verbose_name='precio Total',
    )
    buyer = models.ForeignKey(
        CustomUser,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        unique=False,
        verbose_name='comprador',
    )
    transaction_date_time = models.DateTimeField(
        default=datetime.datetime.now,
        verbose_name='fecha de Transacción',
    )
    paypal_id = models.CharField(
        max_length=64,
        verbose_name='pago Paypal ID'
    )
    payer_id = models.CharField(
        max_length=128,
        blank=True,
        verbose_name='cliente Paypal ID'
    )
    payment_status = models.BooleanField(
        default=False,
        verbose_name='estado de Pago'
    )

    class Meta:
        indexes = [
            models.Index(fields=['id', ], name='transaction_id_index'),
            models.Index(fields=['paypal_id', ], name='paypal_payment_id_index'),
            models.Index(fields=['payer_id', ], name='paypal_payer_id_index')
        ]
        ordering = ['transaction_date_time', ]
        verbose_name = 'transacción'
        verbose_name_plural = 'transacciones'

    def __str__(self):
        return f'NoP: {self.publication_number} for ${self.total_price} on {self.transaction_date_time}'
