from django.test import TestCase

from users.models import Report, CustomUser, Comment
from users.utils import check_mobile_phone_integrity, check_email_integrity


class RegisterNewUserFormMobilePhoneTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        pass

    def setup(self):
        pass

    def test_mobile_phone_fail_1(self):
        mobile_phone = '5530531805044'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_2(self):
        mobile_phone = 'sss5530531805'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_3(self):
        mobile_phone = '5530531805sss'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_4(self):
        mobile_phone = '55305318051'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_5(self):
        mobile_phone = '553053180512'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_6(self):
        mobile_phone = '55305318051234565'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_7(self):
        mobile_phone = '554855'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_8(self):
        mobile_phone = '5530531805123'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_9(self):
        mobile_phone = 'jsuftheuio'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_fail_10(self):
        mobile_phone = 'sjidjshsusiop'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertFalse(integrity)

    def test_mobile_phone_valid_1(self):
        mobile_phone = '5530531805'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertTrue(integrity)

    def test_mobile_phone_valid_2(self):
        mobile_phone = '0445530531805'
        integrity = check_mobile_phone_integrity(mobile_phone)
        self.assertTrue(integrity)

    def test_email_fail_1(self):
        email = ''
        integrity = check_email_integrity(email)
        self.assertFalse(integrity)

    def test_email_valid_1(self):
        email = 'alonso.moreno.acad@gmail.com'
        integrity = check_email_integrity(email)
        self.assertTrue(integrity)
