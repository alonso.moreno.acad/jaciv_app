from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from users.forms import CustomUserCreationForm, CustomUserChangeForm
from users.models import CustomUser, Report, Suggestion, Comment, Transaction, Raiting


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('email', 'username', 'date_joined')
    list_filter = ['date_joined', 'sex']

    add_fieldsets = (
        (None, {
            'fields': ('username', 'password1', 'password2',)
        }),
        ('Personal Information', {
            'fields': ('first_name', 'last_name', 'mobile_phone', 'email', 'sex', 'birth_date', )
        }),
        ('Other', {
            'fields': ('terms_accepted',)
        }),
    )

    fieldsets = (
        (None, {
            'fields': ('username', 'password',)
        }),
        ('Personal Infromation', {
            'fields': ('first_name', 'last_name', 'profile_picture', 'mobile_phone', 'email', 'sex', 'birth_date', )
        }),
        ('Status', {
            'fields': ('available_publications', 'terms_accepted',)
        }),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions',)
        }),
        ('Dates', {
            'fields': ('date_joined', 'last_login',)
        }),
    )


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    model = Report
    list_display = ('category', 'recipient', 'sender', 'slug', 'emission_date')
    list_filter = ['recipient', 'emission_date', 'category', ]

    add_fieldsets = (
        ("Usuarios", {
            'fields': ('recipient', 'sender',)
        }),
        ('Reporte', {
            'fields': ('slug', 'message', 'category',)
        }),
    )

    fieldsets = (
        ("Usuarios", {
            'fields': ('recipient', 'sender',)
        }),
        ('Reporte', {
            'fields': ('slug', 'message', 'category',)
        }),
        ('Fecha', {
            'fields': ('emission_date',)
        }),
    )


@admin.register(Suggestion)
class SuggestionAdmin(admin.ModelAdmin):
    model = Suggestion
    list_display = ('recipient', 'sender', 'slug', 'emission_date')
    list_filter = ['recipient', 'emission_date']

    add_fieldsets = (
        ("Usuarios", {
            'fields': ('recipient', 'sender',)
        }),
        ('Sugerencia', {
            'fields': ('slug', 'message', )
        }),
    )

    fieldsets = (
        ("Usuarios", {
            'fields': ('recipient', 'sender',)
        }),
        ('Sugerencia', {
            'fields': ('slug', 'message', )
        }),
        ('Fecha', {
            'fields': ('emission_date',)
        }),
    )


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    model = Comment
    list_display = ('recipient', 'sender', 'emission_date')
    list_filter = ['recipient', 'emission_date']

    add_fieldsets = (
        ("Usuarios", {
            'fields': ('recipient', 'sender',)
        }),
        ('Comentario', {
            'fields': ('message', )
        }),
    )

    fieldsets = (
        ("Usuarios", {
            'fields': ('recipient', 'sender',)
        }),
        ('Comentario', {
            'fields': ('message', )
        }),
    )


@admin.register(Raiting)
class RaitingAdmin(admin.ModelAdmin):
    model = Raiting
    list_display = ('reciever', 'grade', 'grade_date')
    list_filter = ['reciever']

    add_fieldsets = (
        ("Usuarios", {
            'fields': ('reciever', 'grader',)
        }),
        ('Calificación', {
            'fields': ('grade', )
        }),
    )

    fieldsets = (
        ("Usuarios", {
            'fields': ('reciever', 'grader',)
        }),
        ('Calificación', {
            'fields': ('grade', )
        }),
    )


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    model = Transaction
    list_display = ('publication_number', 'total_price',
                    'buyer', 'transaction_date_time', 'payment_status',)
    list_filter = ['publication_number',
                   'total_price', 'transaction_date_time']

    add_fieldsets = (
        ("Transacción", {
            'fields': ('publication_number', 'total_price', 'buyer', 'transaction_date_time',)
        }),
        ('Paypal', {
            'fields': ('paypal_id', 'payer_id', 'payment_status',)
        }),
    )

    fieldsets = (
        ("Transacción", {
            'fields': ('publication_number', 'total_price', 'buyer', 'transaction_date_time',)
        }),
        ('Paypal', {
            'fields': ('paypal_id', 'payer_id', 'payment_status',)
        }),
    )
