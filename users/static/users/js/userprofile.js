var slider = document.getElementById("gradeControlRange");
var grade = document.getElementById("gradePicker");

grade.value = slider.value;

slider.oninput = function() {
  grade.value = slider.value;
};
