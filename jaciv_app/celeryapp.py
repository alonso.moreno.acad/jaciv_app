import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'jaciv_app.settings')

app = Celery('jaciv_app')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
 