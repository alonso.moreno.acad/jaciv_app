import uuid
import datetime
from dateutil.relativedelta import relativedelta
from django.db import models
from django.urls import reverse

from users.models import CustomUser


class Tag(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
    )
    tag_name = models.CharField(
        max_length=25,
        verbose_name='etiqueta',
    )

    class Meta:
        indexes = [
            models.Index(fields=['id', ], name='tag_id_index')
        ]
        ordering = ['tag_name', ]
        verbose_name = 'etiqueta'
        verbose_name_plural = 'etiquetas'

    def __str__(self):
        return self.tag_name


class Deal(models.Model):

    publication_number = models.IntegerField(
        verbose_name='número de Publicaciones',
    )
    price = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        verbose_name='precio'
    )
    deal_picture = models.ImageField(
        upload_to='images/deals_pics',
        blank=True,
        null=True,
        verbose_name='foto de Oferta',
    )

    class Meta:
        verbose_name = 'promoción'
        verbose_name_plural = 'promociones'

    def __str__(self):
        return f'# {self.publication_number} $ {self.price}'


class Publication(models.Model):

    REMOVAL_OPTIONS = (
        ('V', 'Venta'),
        ('E', 'Expiración'),
        ('O', 'Otro'),
    )

    CURRENT_STATE = (
        ('D', 'Disponible'),
        ('N', 'No Disponible'),
    )

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
    )
    product_name = models.CharField(
        max_length=35,
        verbose_name='nombre del Producto',
    )
    slug = models.CharField(
        max_length=125,
        verbose_name='descripción Corta',
    )
    product_description = models.TextField(
        max_length=500,
        verbose_name='descripción',
    )
    price = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        verbose_name='precio',
    )
    photo = models.ImageField(
        upload_to='images/product_pic',
        verbose_name='foto del Producto',
        help_text='Recomendado: Utilizar imágenes horizontales'
    )
    seller = models.ForeignKey(
        CustomUser,
        on_delete=models.Case,
        blank=True,
        null=True,
        related_name='customuser_as_seller',
        verbose_name='vendedor'
    )
    tag_list = models.ManyToManyField(
        Tag,
        related_name='tag_as_publication_tag',
        verbose_name='etiquetas',
    )
    publication_date = models.DateField(
        default=datetime.datetime.now,
        verbose_name='fecha de Publicación',
    )
    removal_date = models.DateField(
        default=(datetime.date.today() + relativedelta(months=+6)),
        blank=True,
        null=True,
        verbose_name='fecha de Baja',
    )
    removal_motive = models.CharField(
        max_length=1,
        choices=REMOVAL_OPTIONS,
        default='O',
        blank=True,
        null=True,
        verbose_name='motivo de Baja'
    )
    state = models.CharField(
        max_length=1,
        choices=CURRENT_STATE,
        default='D',
        verbose_name='disponibilidad'
    )
    views = models.DecimalField(
        max_digits=12,
        decimal_places=0,
        default=0,
        blank=True,
        null=True,
        verbose_name='vistas',
    )

    class Meta:
        indexes = [
            models.Index(fields=['id', ], name='publication_id_index'),
        ]
        ordering = ['publication_date', 'seller']
        verbose_name = 'publicación'
        verbose_name_plural = 'publicaciones'

    def get_absolute_url(self):
        return reverse('publication-details', args=[str(self.id)])

    def __str__(self):
        return f'{self.product_name} ${self.price}'


class PublicationWarehouse(models.Model):
    REMOVAL_OPTIONS = (
        ('V', 'Venta'),
        ('E', 'Expiración'),
        ('O', 'Otro'),
    )

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
    )
    product_name = models.CharField(
        max_length=35,
        verbose_name='nombre del Producto',
    )
    price = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        verbose_name='precio',
    )
    seller = models.ForeignKey(
        CustomUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='customuser_as_seller_w',
        verbose_name='vendedor'
    )
    publication_date = models.DateField(
        default=datetime.datetime.now,
        verbose_name='fecha de Publicación',
    )
    removal_date = models.DateField(
        default=datetime.datetime.now,
        verbose_name='fecha de Baja',
    )
    active_days = models.IntegerField(
        verbose_name='diferencia de Días',
    )
    removal_motive = models.CharField(
        max_length=1,
        choices=REMOVAL_OPTIONS,
        default='O',
        verbose_name='motivo de Baja'
    )

    class Meta:
        indexes = [
            models.Index(fields=['id', ], name='publication_w_id_index'),
        ]
        ordering = ['removal_date', 'seller']
        verbose_name = 'almacén'
        verbose_name_plural = 'almacenes'

    def __str__(self):
        return f'{self.product_name} ${self.price} DIV: {self.active_days}'
