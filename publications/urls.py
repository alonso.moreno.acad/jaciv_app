from django.urls import path
from publications.views import show_deals, payment_cancel, create_publication
from publications.views import get_account_publications, publication_details, delete_publication
from publications.views import change_publication_state, report_publication
from publications.views import PaypalPaymentProcess, PaypalPaymentResponse

urlpatterns = [
    path(
        'account-publications/',
        get_account_publications,
        name='account-publications',
    ),
    path(
        'change-publication-state/<uuid:pk>',
        change_publication_state,
        name='change-publication-state',
    ),
    path(
        'create-publication/',
        create_publication,
        name='create-publication',
    ),
    path(
        'deals/',
        show_deals,
        name='show-pulication-deals',
    ),
    path(
        'delete-publication',
        delete_publication,
        name='delete-publication',
    ),
    path(
        'payment-cancel/',
        payment_cancel,
        name='payment-cancel',
    ),
    path(
        'payment-done/',
        PaypalPaymentResponse.as_view(),
        name="payment-done",
    ),
    path(
        'process-payment/<int:pk>/',
        PaypalPaymentProcess.as_view(),
        name='process-payment',
    ),
    path(
        'publication-details/<uuid:pk>',
        publication_details,
        name='publication-details',
    ),
    path(
        'report-publication/<uuid:pk>/',
        report_publication,
        name='report-publication',
    ),
]
