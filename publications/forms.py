from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from publications.models import Publication
from publications.utils import check_price_integrity


class BuyPublicationsForm(forms.Form):

    publication_number = forms.IntegerField(
        required=True,
    )
    price = forms.DecimalField(
        max_digits=6,
        decimal_places=2,
    )


class CreatePublicationForm(forms.ModelForm):

    tag_field = forms.CharField(
        max_length=500,
        label='Etiquetas',
        help_text='Separar las etiquetas con [,] ejemplo: microcontrolador, arduino, uno')

    class Meta:
        model = Publication
        fields = ['product_name', 'slug', 'product_description', 'price', 'photo',
                  'tag_field', ]

    def clean_price(self):
        """ Method to verify te integrity of the price  """

        data = self.cleaned_data['price']
        price_is_valid = check_price_integrity(data)

        if price_is_valid is False:
            raise ValidationError(_('Ingrese un número mayor a 0.'))

        return data


class DeletePublicationForm(forms.Form):

    REMOVAL_OPTIONS = (
        ('V', 'Venta'),
        ('E', 'Expiración'),
        ('O', 'Otro'),
    )

    uuid = forms.UUIDField()
    removal_motive = forms.ChoiceField(
        choices=REMOVAL_OPTIONS, label='Selecciona el motivo de la eliminación')

    def __init__(self, *args, **kwargs):
        super(DeletePublicationForm, self).__init__(*args, **kwargs)
        self.fields['removal_motive'].required = True
        self.fields['uuid'].required = True
        self.fields['uuid'].widget = forms.HiddenInput()
