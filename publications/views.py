import logging
import datetime
import unicodedata
import uuid
import paypalrestsdk
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from publications.forms import BuyPublicationsForm, CreatePublicationForm, DeletePublicationForm
from publications.models import Deal, Tag, Publication, PublicationWarehouse
from users.models import CustomUser, Transaction, Report
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import RedirectView, TemplateView
from django.utils.decorators import method_decorator
from jaciv_app import settings
from django.http import HttpResponseBadRequest


class PaypalPaymentProcess(LoginRequiredMixin, RedirectView):
    login_url = '/login/'
    redirect_field_name = 'login'

    def _generate_item_list(self, deal):

        item_name = ''
        if deal.publication_number == 1:
            item_name = f'{deal.publication_number} Publicación'
        else:
            item_name = f'{deal.publication_number} Publicaciones'

        items = []
        items.append({
            'name': item_name,
            'price': ('%.2f' % deal.price),
            'currency': 'MXN',
            'quantity': 1,

        })

        return items

    def _generate_paypal_payment(self, deal):

        payment = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": "http://www.jaciv.tk:8000/publications/payment-done",
                "cancel_url": "http://www.jaciv.tk:8000/publications/payment-cancel",
            },
            "transactions": [{
                "item_list": {
                    "items": self._generate_item_list(deal)
                },
                "amount": {
                    "total": ('%.2f' % deal.price),
                    "currency": "MXN"
                },
                "description": "Pago por compra de publicaciones"
            }]
        }

        return payment

    def _generate_paypal_url(self, deal):

        paypalrestsdk.configure({
            'mode': settings.PAYPAL_MODE,
            'client_id': settings.PAYPAL_CLIENT_ID,
            'client_secret': settings.PAYPAL_CLIENT_SECRET,
        })

        paypal_payment = paypalrestsdk.Payment(
            self._generate_paypal_payment(deal)
        )

        if paypal_payment.create():
            for link in paypal_payment.links:
                if link.method == 'REDIRECT':
                    redirect_url = (link.href)

        return redirect_url, paypal_payment

    def get_redirect_url(self, *args, **kwargs):

        deal = get_object_or_404(Deal, pk=kwargs['pk'])
        payment_url, paypal_payment = self._generate_paypal_url(deal)

        self.request.session['payment_id'] = paypal_payment.id

        new_transaction = Transaction.objects.create(
            publication_number=deal.publication_number,
            total_price=deal.price,
            buyer=self.request.user,
            transaction_date_time=timezone.now(),
            paypal_id=paypal_payment.id,
            payer_id=''
        )

        return payment_url


class PaypalPaymentResponse(LoginRequiredMixin, TemplateView):
    login_url = '/login/'
    redirect_field_name = 'login'
    template_name = 'publications/return_payment.html'

    def _accept_paypal_payment(self, paypal_id, payer_id):
        transaction = get_object_or_404(Transaction, paypal_id=paypal_id)
        paypal_payment = paypalrestsdk.Payment.find(paypal_id)

        if paypal_payment.execute({'payer_id': payer_id}):
            transaction.payment_status = True
            transaction.payer_id = payer_id
            transaction.save()

            user = get_object_or_404(CustomUser, pk=transaction.buyer.id)
            print(f'Publications available before update {user.available_publications}')
            user.available_publications = user.available_publications + \
                transaction.publication_number
            user.save()
            print(f'Publications available after update {user.available_publications}')

        return transaction

    @method_decorator(csrf_exempt)
    def get(self, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        try:
            payer_id = self.request.GET['PayerID']
            paypal_id = self.request.GET['paymentId']
        except Exception:
            raise HttpResponseBadRequest

        transaction = self._accept_paypal_payment(paypal_id, payer_id)

        return self.render_to_response(context)


@login_required(redirect_field_name='login')
def show_deals(request):
    deals = Deal.objects.all().order_by('publication_number')
    context = {
        'deals': deals,
    }
    return render(request, 'publications/publication_deals.html', context=context)


@csrf_exempt
def payment_cancel(request):
    return render(request, 'publications/cancel_payment.html')


@login_required(redirect_field_name='login')
def create_publication(request):
    if request.method == 'POST':
        form = CreatePublicationForm(request.POST, request.FILES)
        if form.is_valid():
            user = request.user
            product_name = form.cleaned_data['product_name']
            slug = form.cleaned_data['slug']
            product_description = form.cleaned_data['product_description']
            price = form.cleaned_data['price']
            photo = form.cleaned_data['photo']
            raw_tags = form.cleaned_data['tag_field']
            raw_tags = unicodedata.normalize('NFKD', raw_tags)
            raw_tags = raw_tags.upper()

            raw_tag_list = raw_tags.split(',')
            tag_list = []

            for tag in raw_tag_list:
                check_tag = Tag.objects.filter(tag_name=tag).count()
                if check_tag > 0:
                    check_tag = Tag.objects.get(tag_name=tag)
                    tag_list.append(check_tag)
                else:
                    Tag.objects.create(tag_name=tag)
                    tag_to_add = Tag.objects.get(tag_name=tag)
                    tag_list.append(tag_to_add)

            new_publication = Publication.objects.create(
                product_name=product_name,
                slug=slug,
                product_description=product_description,
                price=price,
                photo=photo,
                seller=user,
            )
            new_publication = new_publication.tag_list.set(tag_list)

            user.available_publications = user.available_publications - 1
            user.save()
            return redirect('account-publications')
        else:
            user = request.user
            context = {
                'user': user,
                'form': form,
            }
            return render(request, 'publications/create_publication.html', context=context)
    else:
        user = request.user
        form = CreatePublicationForm()
        context = {
            'user': user,
            'form': form,
        }
        return render(request, 'publications/create_publication.html', context=context)


@login_required(redirect_field_name='login')
def get_account_publications(request):
    user = request.user
    user_publications = Publication.objects.filter(seller=user)

    paginator = Paginator(user_publications, 10)
    page = request.GET.get('page')
    publications = paginator.get_page(page)

    context = {
        'publications': publications,
    }

    return render(request, 'publications/account_publications.html', context=context)


def publication_details(request, pk):
    user = request.user
    publication = get_object_or_404(Publication, pk=pk)
    seller = CustomUser.objects.get(pk=publication.seller.id)
    removal_form = DeletePublicationForm(initial={
        'uuid': pk,
    })

    publication.views = publication.views + 1
    publication.save()

    context = {
        'user': user,
        'seller': seller,
        'publication': publication,
        'removal_form': removal_form,
    }

    return render(request, 'publications/publication_details.html', context=context)


@login_required(redirect_field_name='login')
def change_publication_state(request, pk):
    publication = get_object_or_404(Publication, pk=pk)

    if publication.state == 'D':
        publication.state = 'N'
    elif publication.state == 'N':
        publication.state = 'D'

    publication.save()

    return redirect('publication-details', pk=publication.id)


@login_required(redirect_field_name='login')
def delete_publication(request):
    if request.method == 'POST':

        form = DeletePublicationForm(request.POST)

        if form.is_valid():

            deletion_id = form.cleaned_data['uuid']
            deletion_motive = form.cleaned_data['removal_motive']

            deletion_date = datetime.datetime.now()
            publication_to_delete = Publication.objects.get(pk=deletion_id)
            active_days = get_days_passed(
                publication_to_delete.publication_date, deletion_date)
            publication_old = PublicationWarehouse.objects.create(
                product_name=publication_to_delete.product_name,
                price=publication_to_delete.price,
                seller=publication_to_delete.seller,
                publication_date=publication_to_delete.publication_date,
                removal_date=deletion_date,
                active_days=active_days,
                removal_motive=deletion_motive,
            )
            publication_old.save()
            publication_to_delete.delete()

            return redirect('account-publications')

        else:
            pass

    else:
        pass


@login_required(redirect_field_name='login')
def report_publication(request, pk):
    user = request.user
    publication = Publication.objects.get(pk=pk)

    report = Report.objects.create(
        sender=user,
        recipient=publication.seller,
        slug='Reporte de Publicacón',
        message=publication.id,
        category='P'
    )

    report.save()

    return redirect('publication-details', pk=publication.id)


def get_days_passed(before, after):
    d1 = before
    d2 = after.date()
    return abs(d1 - d2).days
