from django.contrib import admin

from publications.models import Tag, Publication, PublicationWarehouse, Deal


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    model = Tag
    list_display = ['tag_name']
    add_fieldsets = (
        ('Etiqueta', {
            'fields': ('tag_name',),
        }),
    )
    fieldsets = (
        ('Etiqueta', {
            "fields": ('tag_name', ),
        }),
    )


@admin.register(Deal)
class DealAdmin(admin.ModelAdmin):
    model = Deal
    list_display = ['publication_number', 'price']
    add_fieldsets = (
        ('Promoción', {
            'fields': ('publication_number', 'price', 'deal_picture',),
        }),
    )
    fieldsets = (
        ('Promoción', {
            'fields': ('publication_number', 'price', 'deal_picture',),
        }),
    )


@admin.register(Publication)
class PublicationAdmin(admin.ModelAdmin):
    model = Publication
    list_display = ['product_name', 'price', 'seller', 'publication_date', ]
    list_filter = ['price', 'publication_date', ]
    add_fieldsets = (
        ('Producto', {
            'fields': ('product_name', 'price', 'seller'),
        }),
        ('Detalles', {
            'fields': ('photo', 'slug', 'product_description', 'tag_list'),
        }),
        ('Fechas', {
            'fields': ('publication_date', ),
        }),
    )
    fieldsets = (
        ('Producto', {
            'fields': ('product_name', 'price', 'seller'),
        }),
        ('Detalles', {
            'fields': ('photo', 'slug', 'product_description', 'tag_list', 'removal_motive', 'views', 'state', ),
        }),
        ('Fechas', {
            'fields': ('publication_date', 'removal_date'),
        }),
    )


@admin.register(PublicationWarehouse)
class PublicationWarehouseAdmin(admin.ModelAdmin):
    model = PublicationWarehouse
    list_display = ['product_name', 'price', 'seller',
                    'publication_date', 'removal_date', ]
    list_filter = ['price', 'publication_date',
                   'removal_date', 'removal_motive', ]
    add_fieldsets = (
        ('Producto', {
            'fields': ('product_name', 'price', 'seller'),
        }),
        ('Detalles', {
            'fields': ('removal_motive', 'active_days',),
        }),
        ('Fechas', {
            'fields': ('publication_date', 'removal_date', ),
        }),
    )
    fieldsets = (
        ('Producto', {
            'fields': ('product_name', 'price', 'seller'),
        }),
        ('Detalles', {
            'fields': ('removal_motive', 'active_days',),
        }),
        ('Fechas', {
            'fields': ('publication_date', 'removal_date', ),
        }),
    )
