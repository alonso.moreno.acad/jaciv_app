from django.test import TestCase
from publications.utils import check_price_integrity


class RegisterNewPublicationTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print('Runing setup for tests')
        pass

    def setup(self):
        print('Runing setup for tests')
        pass

    def test_price_integrity_fail(self):
        price_to_test_list = [0, -5]

        for price in price_to_test_list:
            with self.subTest(price):
                is_valid = check_price_integrity(price)
                self.assertFalse(is_valid)

    def test_price_integrity_success(self):
        price = 10
        is_valid = check_price_integrity(price)
        self.assertTrue(is_valid)
