import datetime

from celery import shared_task
from publications.models import Publication
from users.models import Comment, CustomUser


#@shared_task
#def test_celery_every_thirty_seconds():
#    user_sender = CustomUser.objects.get(pk=2)
#    user_recipient = CustomUser.objects.get(pk=1)
#    new_comment = Comment.objects.create(
#        sender=user_sender,
#        recipient=user_recipient,
#        message='Comentario Celery Prueba'
#    )
#    new_comment.save()


@shared_task
def remove_due_publications():
    publications_to_delete = Publication.objects.get(
        removal_date=datetime.date.today())

    deletion_date = datetime.datetime.now()

    for publication_to_delete in publications_to_delete:

        active_days = get_days_passed(
            publication_to_delete.publication_date, deletion_date)

        publication_old = PublicationWarehouse.objects.create(
            product_name=publication_to_delete.product_name,
            price=publication_to_delete.price,
            seller=publication_to_delete.seller,
            publication_date=publication_to_delete.publication_date,
            removal_date=deletion_date,
            active_days=active_days,
            removal_motive="E",
        )
        publication_old.save()
        publication_to_delete.delete()


def get_days_passed(before, after):
    d1 = before
    d2 = after.date()
    return abs(d1 - d2).days
